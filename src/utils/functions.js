// creates a vertex or fragment shader, 
// it needs the gl instance, type of shader
// and glsl source
const createShader = (gl, type, source) => {
  const shader = gl.createShader(type)
  gl.shaderSource(shader, source)
  gl.compileShader(shader)
  if (gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    return shader
  }
  console.log(gl.getShaderInfoLog(shader))
  gl.deleteShader(shader)
}

// creates a program, it need the gl instance, a
// vetex shader and a fragment shader
const createProgram = (gl, vertexShader, fragmentShader) => {
  const program = gl.createProgram()
  gl.attachShader(program, vertexShader)
  gl.attachShader(program, fragmentShader)
  gl.linkProgram(program)
  if (gl.getProgramParameter(program, gl.LINK_STATUS)) {
    return program
  }
  console.log(gl.getProgramInfoLog(program))
  gl.deleteProgram(program)
}
// creates a canvas element, with gl context
const createCanvasGL = (el, width, height) => {
  const canvas = document.createElement('canvas')
  canvas.width = width
  canvas.height = height
  el.append(canvas)
  let gl = canvas.getContext('webgl')
  if (!gl) {
    console.log('trying to use experimental webgl')
    gl = canvas.getContext('experimental-webgl')
  }
  if (gl) {
    return gl
  }
  throw new Error('Browser does not support WEBGL')
}

export {
  createShader,
  createProgram,
  createCanvasGL
}
