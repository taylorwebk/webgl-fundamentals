import {
  createProgram, createShader, createCanvasGL
} from './utils/functions'

import vertexSrc from './vertexs/one.vs.glsl'
import shaderSrc from './fragments/one.fs.glsl'

const gl = createCanvasGL(
  document.querySelector('body'),
  500,
  500
)

const vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexSrc)
const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, shaderSrc)

const program = createProgram(gl, vertexShader, fragmentShader)
// name of variable that handles the vertex position
const positionAttribLoc = gl.getAttribLocation(program, 'a_position')
const positionBuffer = gl.createBuffer()

gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer)
const positions = [
  0, 1,
  0.1123, 0.6545,
  0.4754, 0.6545,
  0.1816, 0.4411,
  0.2939, 0.0956,
  0, 0.3091,
  -0.2939, 0.0956,
  -0.1816, 0.4411,
  -0.4754, 0.6545,
  -0.1123, 0.6545,
  0,1
]

gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW)

// render part
gl.clearColor(0, 0, 0, 0.22)
gl.clear(gl.COLOR_BUFFER_BIT)
gl.useProgram(program)
gl.enableVertexAttribArray(positionAttribLoc)
gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer)

const size = 2
const type = gl.FLOAT
const normalize = false
const stride = 0
const offset = 0
gl.vertexAttribPointer(
  positionAttribLoc, size, type, normalize, stride, offset
)

const primitiveType = gl.LINE_STRIP
const offsetr = 0
let count = 11
gl.drawArrays(primitiveType, offsetr, count)


const positions2 = [
  0, 0,
  0.1123, -0.3455,
  0.4754, -0.3455,
  0.1816, -0.5589,
  0.2939, -0.9044,
  0, 0.3091 - 1,
  -0.2939, -0.9044,
  -0.1816, -0.5589,
  -0.4754, -0.3455,
  -0.1123, -0.3455,
  0,0,
  0, -0.5,
  0.1123, -0.3455,
  0, -0.5,
  0.4754, -0.3455,
  0, -0.5,
  0.1816, -0.5589,
  0, -0.5,
  0.2939, -0.9044,
  0, -0.5,
  0, 0.3091 - 1,
  0, -0.5,
  -0.2939, -0.9044,
  0, -0.5,
  -0.1816, -0.5589,
  0, -0.5,
  -0.4754, -0.3455,
  0, -0.5,
  -0.1123, -0.3455,
  0, -0.5
]
count = 30

gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions2), gl.STATIC_DRAW)
gl.drawArrays(primitiveType, offsetr, count)