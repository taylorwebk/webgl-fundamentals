// a_position will receive data from a buffer
attribute vec4 a_position;
// main function
void main() {
  // gl_Position is a variable to set position
  gl_Position = a_position;
}